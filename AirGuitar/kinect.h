#ifndef KINECT_H
#define KINECT_H

// KinectSDK
#include <windows.h>
#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>

// GLUT
#include <gl/glut.h>

#include "geom.h"

class Sensor {
private:
  HANDLE bgra_cam;
  HANDLE skeleton_updated;
  NUI_SKELETON_DATA skeleton;
  INuiSensor* device;  
  unsigned int x_res;
  unsigned int y_res;
  Point2D L_hand;
  Point2D R_hand;
  Point2D L_shoulder;
  Point2D R_shoulder;
  Point2D body;

  // convert from skeleton coordinates to pixel coordinates
  Point2D skel_to_pix(Vector4 pos);

public:
  Sensor();
  
  // store bgra frame in frame_buffer
  void get_bgra_frame(GLubyte* frame_buffer);

  // check for (and store) updates to hand positions
  void update_skeleton();

  // get the left hand position vector
  Point2D get_L_hand();

  // get the right hand position vector
  Point2D get_R_hand();

  // get the left shoulder position vector
  Point2D get_L_shoulder();

  // get the right shoulder position vector
  Point2D get_R_shoulder();

  // get the body position vector
  Point2D get_body();
};

#endif

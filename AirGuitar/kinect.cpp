/* Sensor IO layer */

#include "kinect.h"

// KinectSDK
#include <windows.h>
#include <NuiApi.h>
#include <NuiImageCamera.h>
#include <NuiSensor.h>

// GLUT
#include <gl/glut.h>

#include "geom.h"

Sensor::Sensor() {
  x_res = 640; // TODO: put in seperate constants file
  y_res = 480;
  
  // init first found device sensor
  NuiCreateSensorByIndex(0, &device);
  HRESULT initd = device->NuiInitialize(NUI_INITIALIZE_FLAG_USES_COLOR 
					| NUI_INITIALIZE_FLAG_USES_SKELETON);
  if (! SUCCEEDED(initd)) return; // TODO: error handling

  // init color camera
  device->NuiImageStreamOpen(NUI_IMAGE_TYPE_COLOR,
			     NUI_IMAGE_RESOLUTION_640x480,
			     NUI_IMAGE_DEPTH_NO_VALUE,
			     2,        // double-buffering
			     NULL,     // do nothing on reset
			     &bgra_cam);

  // init skeleton camera
  skeleton_updated = CreateEventW(NULL, true, false, NULL); // initially false
  device->NuiSkeletonTrackingEnable(skeleton_updated, 
				    NUI_SKELETON_TRACKING_FLAG_ENABLE_SEATED_SUPPORT);
}


void Sensor::update_skeleton() {
  // quick test idea from MS kinect docs
  if ( WAIT_OBJECT_0 == WaitForSingleObject(skeleton_updated, 0) ) {
    // try to load frame
    NUI_SKELETON_FRAME skeleton_frame = {0};
    if (SUCCEEDED(device->NuiSkeletonGetNextFrame(0, &skeleton_frame))) {
      // smoothing filter
      // re parameters: msdn.microsoft.com/en-us/library/jj131024.aspx
      NUI_TRANSFORM_SMOOTH_PARAMETERS default_MSDN = {0.5f, 0.5f, 0.5f, 0.05f, 0.04f};
      device->NuiTransformSmooth(&skeleton_frame, &default_MSDN);

      // loop through available skeletons and update
      for (int i=0; i<NUI_SKELETON_COUNT; i++) {
	const NUI_SKELETON_DATA &found_skeleton = skeleton_frame.SkeletonData[i];

	if (found_skeleton.eTrackingState != NUI_SKELETON_NOT_TRACKED) {
	  skeleton = found_skeleton;
	}
      }      
    }
  }
}


Point2D Sensor::skel_to_pix(Vector4 pos) {
  Point2D pix_pos;  
  GLfloat x,y;
  // per SkeletopBasics example, transform sends to 320x240
  NuiTransformSkeletonToDepthImage(pos, &x, &y);
  pix_pos.x = (x*640)/320.0f;
  pix_pos.y = (y*480)/240.0f;
  return pix_pos;
}


Point2D Sensor::get_L_hand() {
  NUI_SKELETON_POSITION_TRACKING_STATE state = 
    skeleton.eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_HAND_LEFT];
  if (state != NUI_SKELETON_POSITION_NOT_TRACKED) {
    L_hand = this->skel_to_pix(skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_LEFT]);
  }   
  return L_hand;
}


Point2D Sensor::get_R_hand() {
  NUI_SKELETON_POSITION_TRACKING_STATE state = 
    skeleton.eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_HAND_RIGHT];
  if (state != NUI_SKELETON_POSITION_NOT_TRACKED) {
    R_hand = this->skel_to_pix(skeleton.SkeletonPositions[NUI_SKELETON_POSITION_HAND_RIGHT]);
  }   
  return R_hand;
}


Point2D Sensor::get_body() {
  NUI_SKELETON_POSITION_TRACKING_STATE state = 
    skeleton.eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_SHOULDER_CENTER];
  if (state != NUI_SKELETON_POSITION_NOT_TRACKED) {
    body = this->skel_to_pix(skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_CENTER]);
  }   
  return body;
}


Point2D Sensor::get_R_shoulder() {
  NUI_SKELETON_POSITION_TRACKING_STATE state = 
    skeleton.eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_SHOULDER_RIGHT];
  if (state != NUI_SKELETON_POSITION_NOT_TRACKED) {
    R_shoulder = this->skel_to_pix(skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_RIGHT]);
  }   
  return R_shoulder;  
}

Point2D Sensor::get_L_shoulder() {
  NUI_SKELETON_POSITION_TRACKING_STATE state = 
    skeleton.eSkeletonPositionTrackingState[NUI_SKELETON_POSITION_SHOULDER_LEFT];
  if (state != NUI_SKELETON_POSITION_NOT_TRACKED) {
    L_shoulder = this->skel_to_pix(skeleton.SkeletonPositions[NUI_SKELETON_POSITION_SHOULDER_LEFT]);
  }   
  return L_shoulder;  
}


void Sensor::get_bgra_frame(GLubyte* frame_buffer) {
  NUI_IMAGE_FRAME image_frame;
  NUI_LOCKED_RECT locked_rect;
  
  // obtain frame, returning if unsuccessful
  int timeout_wait = 0; // ms
  HRESULT frame_result = device->NuiImageStreamGetNextFrame(bgra_cam, timeout_wait, &image_frame);
  if ( frame_result != S_OK ) {
    return;
  }
  
  INuiFrameTexture* texture = image_frame.pFrameTexture;
  texture->LockRect(0, &locked_rect, NULL, 0);
  
  if (locked_rect.Pitch != 0) // if frame is not empty
    {
      for (unsigned int i=0; i<x_res*y_res*4; i++) {
	frame_buffer[i] = locked_rect.pBits[i];
      }
    }
  texture->UnlockRect(0);
  device->NuiImageStreamReleaseFrame(bgra_cam, &image_frame);
}

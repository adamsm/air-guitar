#ifndef GEOM_H
#define GEOM_H

#include <gl/glut.h>

const GLfloat GEOM_PRECISION = 0.000001f;

// a simple 2d point class
class Point2D {
public:
  GLfloat x;
  GLfloat y;
  Point2D(GLfloat x_=0.0, GLfloat y_=0.0);
};

// return true if the line segments given by those two points intersect
// sets the intersection parameter if it is found
bool line_segs_intersect(Point2D a0, Point2D a1, Point2D b0, Point2D b1);

#endif

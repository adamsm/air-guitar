#ifndef STRINGINST_H
#define STRINGINST_H

#include "Noise.h"
#include "Delay.h"
#include "DelayA.h"
#include "OnePole.h"
#include "Stk.h"

#include <math.h>

const float HALF_STEP  = pow(2.0f, 1.0f/12.0f);

class String {
 private:
  // general properties
  float freq;
  int burst_len;

  // intermediate values
  stk::StkFloat sample;
  stk::StkFloat tick_out;

  /* signal chain components + their properties, in order */

  // generate initial burst of noise
  stk::Noise noise;
  stk::Delay burst;

  // modify noise based on attack
  stk::OnePole attack_filter;

  // delay to mimic wave propagation
  stk::DelayA delay;
  stk::StkFloat delay_len;

  // filter to mimic dampening on reflection
  stk::OnePole damp_filter;
  stk::StkFloat damp_pole;

 public:
  String(float frequency = 440.0);
  void set_freq(float frequency);
  void pluck(float pct_attack);
  void print();
  stk::StkFloat tick();
};



class Strings {
 private:
  std::vector<String> strings_vec;
  std::vector<float> pitch_offset_factors;
  unsigned int num_strings;

 public:
  Strings(unsigned int count);

  // set a string's pitch to the given frequency (will be modified by
  // the string's relative pitch)
  void set_string_pitch(unsigned int string_idx, float freq);
  
  // set the difference between a pitch and its inputted frequencies
  void set_relative_string_pitch(unsigned int string_idx, int num_half_steps);

  // register a pluck event
  void pluck(unsigned int string_idx, float pct_attack);

  // print information about this set of strings to the terminal
  void print();

  // audio frame update method
  stk::StkFloat tick();
};

#endif

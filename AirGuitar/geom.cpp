#include "geom.h"
#include <gl/glut.h>
#include <math.h>
#include <algorithm>

Point2D::Point2D(GLfloat x_, GLfloat y_) {
  x = x_;
  y = y_;
}

// currently unused, but I've kept it around since it would help with more
// advanced interactions
bool line_segs_intersect(Point2D a0, Point2D a1, Point2D b0, Point2D b1) {
  // using matrix approach with standard form A*x + B*y = C
  // explained here: community.topcoder.com/tc?module=Static&d1=tutorials&d2=geometry2
  GLfloat Aa = a1.y - a0.y;
  GLfloat Ba = a1.x - a0.x;
  GLfloat Ca = Aa*a1.x + Ba*a1.y;

  GLfloat Ab = b1.y - b0.y;
  GLfloat Bb = b1.x - b0.x;
  GLfloat Cb = Ab*b1.x + Bb*b1.y;

  GLfloat det = Aa*Bb - Ba*Ab;
  if (abs(det) < GEOM_PRECISION) {
    return false;
  } 
  
  // intersection found; now check whether intersection happens on both segments
  GLfloat x = (Bb*Ca - Ba*Cb)/det;
  GLfloat max_x = std::max(a0.x, b0.x);
  GLfloat min_x = std::min(a0.x, b0.x);
  if (! (x < max_x && x > min_x ) ) return false;

  GLfloat y = (Aa*Cb - Ab*Ca)/det;
  GLfloat max_y = std::max(a0.y, b0.y);
  GLfloat min_y = std::min(a0.y, b0.y);
  if (! (y < max_y && y > min_y ) ) return false;

  // they intersect
  return true;
}

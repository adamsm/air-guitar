/*
  This is a modified Karplus-Strong string simulation.
  It implements several of the features in:

  Jaffe and Smith. "Extensions of the Karplus-Strong Plucked-String Algorithm."
      Computer Music Journal, Vol. 7, No. 2.
 */

#include "stringInst.h"

// STK
#include "Noise.h"
#include "Delay.h"
#include "DelayA.h"
#include "OnePole.h"

#include <math.h>

/* String */
String::String(float frequency) {
  this->set_freq(frequency);
  damp_pole = 0.65f; // sustain, realistic value found from much experimentation
  damp_filter.setPole(damp_pole);
  burst_len = int(0.002f*stk::Stk::sampleRate());
}

void String::set_freq(float frequency) {
  freq = frequency;

  // set the delay amount to match the pitch
  // (the period of the wave = sum of phase delays = delay_len + 1/2 )
  delay_len = (stk::Stk::sampleRate()/freq) - 0.5f;
  delay.setDelay(delay_len);
}

void String::pluck(float pct_attack) {
  attack_filter.setPole(1.0-pct_attack);
  burst.setDelay(burst_len);

  for (int i=0; i<burst_len; i++) {
    burst.tick(attack_filter.tick(noise.tick()));
  }
}

stk::StkFloat String::tick() {
  sample = burst.tick(0.0);
  tick_out = sample + damp_filter.tick(delay.nextOut());
  delay.tick(tick_out);
  return tick_out;
}

void String::print() {
  std::cout << "String: " << freq << " Hz" << std::endl;
}


/* Strings */
Strings::Strings(unsigned int number) {
  num_strings = number;
  strings_vec.resize(num_strings);
  pitch_offset_factors.resize(num_strings);
}

void Strings::print() {
  for (unsigned int i=0; i<num_strings; i++) {
    strings_vec[i].print();
  }
}

void Strings::set_string_pitch(unsigned int string_idx, float freq) {
  strings_vec[string_idx].set_freq(freq*pitch_offset_factors[string_idx]);
}

void Strings::set_relative_string_pitch(unsigned int string_idx, int num_half_steps) {
  pitch_offset_factors[string_idx] = pow(HALF_STEP, num_half_steps);
}

void Strings::pluck(unsigned int string_idx, float pct_attack) {
  strings_vec[string_idx].pluck(pct_attack);
}

stk::StkFloat Strings::tick() {
  // avg sound from each string
  stk::StkFloat aggregate = 0;
  for (unsigned int s=0; s<num_strings; s++) {
    aggregate += strings_vec[s].tick()/num_strings;
  }
  return aggregate;
}

/* 
   Air Guitar
   Matthew Adams
*/
#define NOMINMAX // prevents windows.h from messing with min() and max()
#include <windows.h>
#include <math.h>
#include <random>
#include <algorithm>

#include <gl/glut.h>
#include "RtAudio.h"

#include "geom.h"
#include "kinect.h"
#include "stringInst.h"

/*
 * SETUP
 */

// params
const float sample_rate = 44100.0;
const short x_res = 640; // do not change - fixed by image from kinect
const short y_res = 480;

const bool PARTY_MODE = false;
const bool FRETLESS_MODE = false;

// pitch_len_factor/string_length = frequency
// scale factor found through experimenting and trying to make the natural
// guitar length match open E2
const float pitch_len_factor = 220;


// global inits that draw() needs to see
// (GLUT seemingly can't register a member draw func, so no OO niceness...)
Sensor sensor;
Strings* guitar;
Point2D prev_pick;
Point2D prev_nut;
Point2D prev_bridge;


// buffer to store bgra_frames
GLuint bgra_texture;
GLubyte bgra_buffer[4*x_res*y_res];


/*
 * GUI FUNCTIONS
 */


// draw the contents of the buffer, using a textured quad for speed
void draw_bgra_texture() {
  glEnable(GL_TEXTURE_2D);

  glBindTexture(GL_TEXTURE_2D, bgra_texture);
  sensor.get_bgra_frame(bgra_buffer);
  glTexSubImage2D(GL_TEXTURE_2D, 
		  0, 0, 0,          // level, x offset, y offset
		  x_res, y_res,         // width, height
		  GL_BGRA_EXT,      // format output by kinect
		  GL_UNSIGNED_BYTE, // (output format)
		  (GLvoid*)bgra_buffer);

  glClear(GL_COLOR_BUFFER_BIT);
  glBegin(GL_QUADS);
  glTexCoord2f(0.0f, 0.0f);
  glVertex3f(0, 0, 0);

  glTexCoord2f(1.0f, 0.0f);
  glVertex3f(x_res, 0, 0);

  glTexCoord2f(1.0f, 1.0f);
  glVertex3f(x_res, y_res, 0.0f);

  glTexCoord2f(0.0f, 1.0f);
  glVertex3f(0, y_res, 0.0f);
  glEnd();

  glDisable(GL_TEXTURE_2D);
}


// helper to rotate point to correct position in the string frame
// this is only accurate for the first quadrant
inline Point2D sf_rotate(Point2D string_nut, Point2D string_bridge, Point2D pt) {
  if ( abs(string_nut.x - string_bridge.x) < GEOM_PRECISION ){
    return pt;
  }
  GLfloat th = atan2((string_bridge.y - string_nut.y),
		     (string_bridge.x - string_nut.x));
  //GLfloat th_deg = th * 180 / 3.14159;
  Point2D sf_pt(pt.x*cos(-th) - pt.y*sin(-th),
		pt.x*sin(-th) + pt.y*cos(-th));
  return sf_pt;
} 


void draw_guitar() {
  Point2D left = sensor.get_L_hand();
  Point2D right = sensor.get_R_hand();
  Point2D body = sensor.get_body();
  float shoulder_width = sensor.get_R_shoulder().x - sensor.get_L_shoulder().x;

  // experimental string positions, aka where I hold my bass guitar
  Point2D nut(left.x, left.y);
  Point2D bridge(body.x+shoulder_width, body.y+1.8f*shoulder_width);
  float string_dist = shoulder_width/30.0f;

  // drop guitar if left hand goes lower than a touch above parallel
  if (left.y > bridge.y-shoulder_width/10.0f) return;

  // draw strings
  glBegin(GL_LINES);

  // draw parallel line segments, that are squared at the ends
  float string_dx = nut.x - bridge.x;
  float string_dy = nut.y - bridge.y;
  float string_len = sqrt(string_dx*string_dx + string_dy*string_dy);
  float string_delx = -string_dy*string_dist/string_len;
  float string_dely = string_dx*string_dist/string_len;
  for (int s=0; s<6; s++) {
    glVertex3f(nut.x + string_delx*s,    nut.y+string_dely*s,      0.0f);
    glVertex3f(bridge.x + string_delx*s, bridge.y + string_dely*s, 0.0f);
  }
  
  // draw end caps
  glVertex3f(nut.x, nut.y, 0.0f);
  glVertex3f(nut.x + string_delx*5, nut.y + string_dely*5, 0.0f);

  glVertex3f(bridge.x, bridge.y, 0.0f);
  glVertex3f(bridge.x + string_delx*5, bridge.y + string_dely*5, 0.0f);
  glEnd();

  // draw pick
  glBegin(GL_QUADS);
  float side = 10;
  glVertex3f(right.x,      right.y,      0.0f);
  glVertex3f(right.x+side, right.y,      0.0f);
  glVertex3f(right.x+side, right.y+side, 0.0f);
  glVertex3f(right.x,      right.y+side, 0.0f);
  glEnd();
  
  glFlush();


  // determine whether any strings were plucked
  // sf_ is a shorthand for string frame- the reference frame where
  // the bottom string lies on the x-axis from (0,0) to (length,0)
  // note that distances are preserved by this transformation

  // first transform to reference frame
  Point2D sf_nut    = sf_rotate(nut, bridge, nut);
  Point2D sf_bridge = sf_rotate(nut, bridge, bridge);
  Point2D sf_pick   = sf_rotate(nut, bridge, right);
  GLfloat y_offset  = -(sf_bridge.y + sf_nut.y)/2.0f;
  sf_nut.y    += y_offset;
  sf_bridge.y += y_offset;
  sf_pick.y   += y_offset;
  GLfloat x_offset = -sf_nut.x;
  sf_nut.x    += x_offset;
  sf_bridge.x += x_offset;
  sf_pick.x   += x_offset;

  Point2D sf_prev_nut    = sf_rotate(prev_nut, prev_bridge, prev_nut);
  Point2D sf_prev_bridge = sf_rotate(prev_nut, prev_bridge, prev_bridge);
  Point2D sf_prev_pick   = sf_rotate(prev_nut, prev_bridge, prev_pick);
  GLfloat prev_y_offset  = -(sf_prev_bridge.y + sf_prev_nut.y)/2.0f;
  sf_prev_nut.y    += prev_y_offset;
  sf_prev_bridge.y += prev_y_offset;
  sf_prev_pick.y   += prev_y_offset;
  GLfloat prev_x_offset = -sf_prev_nut.x;
  sf_prev_nut.x    += prev_x_offset;
  sf_prev_bridge.x += prev_x_offset;
  sf_prev_pick.x   += prev_x_offset;

  // check each string for crossing adn update
  for (int string_num=0; string_num<6; string_num++) {
    if (FRETLESS_MODE) {
      // update pitch based on length (accounting for shoulder width so depth doesn't change pitch)
      guitar->set_string_pitch(5 - string_num, pitch_len_factor / (string_len / shoulder_width));
    }
    
    // useful offset
    GLfloat string_y = -string_num*string_dist;
    
    // quick check to see if string was crossed
    bool string_crossed = false;
    bool plucked_down = false;
    if (sf_pick.y > string_y && sf_prev_pick.y < string_y) { // down
      string_crossed = true;
      plucked_down = true;
    } else if (sf_pick.y < string_y && sf_prev_pick.y > string_y) {  // up
      string_crossed = true;	
      plucked_down = false;
    }  

    // init pick distance travelled
    float pick_dist = GEOM_PRECISION;

    // calculate string intersection of pick's motion in sf frame
    // to determine if there was a collision
    if (string_crossed) {
      // calculate string intercept
      GLfloat pick_slope = (sf_pick.y - sf_prev_pick.y) / (sf_pick.x - sf_prev_pick.x);
      GLfloat x_intercept = -sf_pick.y/pick_slope + sf_pick.x;
      GLfloat string_intercept = x_intercept + string_y/pick_slope;

      // calculate pick distance travelled
      // originally I calculated speed (using wall time), but the timesteps were 
      // just too short; with a decent framerate, this is approx. proportional
      pick_dist = sqrt( pow(sf_pick.x-sf_prev_pick.x, 2) + 
			pow(sf_pick.y-sf_prev_pick.y, 2));

      // using sf_bridge as length is not technically correct, but works in practice
      //  and is much more efficient than calculating the length at strike time
      if (string_intercept > 0 && string_intercept < sf_bridge.x) { 
	string_crossed = true;
      } else {
	string_crossed = false;
      }
    }

    
    /* 
       handle all string crossing events here 
       (string_crossed isn't necessarily correct before this)
    */
    if (string_crossed) {
      // play note
      // velocity gathered from some experimentation and the intuition
      // that sweeping accross the whole frame is as big as you can go
      float vel = std::max(std::min((pick_dist/y_res), 0.999999f), 0.00001f);
      if (! FRETLESS_MODE) {
	// update pitch based on length (accounting for shoulder width so depth doesn't change pitch)
	guitar->set_string_pitch(5 - string_num, pitch_len_factor / (string_len/shoulder_width));
      }
      guitar->pluck(5-string_num, vel);

      if (PARTY_MODE) {
	// flash color on collision
	GLfloat rred = (float)rand()/RAND_MAX;
	GLfloat rgreen = (float)rand()/RAND_MAX;
	GLfloat rblue = (float)rand()/RAND_MAX;
	if (plucked_down) {
	  glColor3f(rred,rgreen,rblue);
	} else {
	  glColor3f(rred,rgreen,rblue);
	}
      }
    }

  }

  // update previous frame points  
  prev_pick = right;
  prev_nut = nut;
  prev_bridge = bridge;
}



// draw a frame (called by GLUT; responsible for triggering other updates)
void draw() {
  sensor.update_skeleton();
  draw_bgra_texture();
  draw_guitar();
  glutSwapBuffers();
}


// start running the GUI
void launch_gui(int argc, char* argv[], char title[]) {
  // GLUT
  glutInit(&argc, argv);
  glutInitWindowPosition(-1,-1); // Let window manager pick
  glutInitWindowSize(x_res, y_res);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow(title);
  glutDisplayFunc(draw);
  glutIdleFunc(draw);
 
  // set up texture used to display the video
  // based off of: cs.princeton.edu/~edwardz/tutorials/
  // (gave idea to use texture directly from the kinect)
  glGenTextures(1, &bgra_texture);
  glBindTexture(GL_TEXTURE_2D, bgra_texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 
	       0, 
	       GL_RGBA8, 
	       x_res, y_res, 
	       0, 
	       GL_BGRA_EXT, GL_UNSIGNED_BYTE, 
	       (GLvoid*) bgra_buffer);
  glBindTexture(GL_TEXTURE_2D, 0);
  glClearColor(0,0,0,0);
  glClearDepth(1.0f);
  
  // ortho view to look directly at full screen rect
  glViewport(0, 0, x_res, y_res);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, x_res, y_res, 0, 1, -1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // run
  glutMainLoop();
}


/*
 * AUDIO
 */

// function called when system wants to update audio buffer
// (modified from examples in STK docs)
int tick( void *outputBuffer, void *inputBuffer, unsigned int nBufferFrames,
	  double streamTime, RtAudioStreamStatus status, void *dataPointer ) {
  register stk::StkFloat *samples = (stk::StkFloat *) outputBuffer;

  for ( unsigned int i=0; i<nBufferFrames; i++ )
    *samples++ = guitar->tick();
  return 0;
}


/*
 * MAIN
 */
int main(int argc, char* argv[]) {
  // start the pick off and to the right so it doesn't play anything when first grabbed
  prev_pick.x = 1000;
  prev_pick.y = -10;

  // init stk audio
  stk::Stk::setSampleRate(sample_rate);
  stk::Stk::showWarnings(false);

  RtAudio output;
  RtAudio::StreamParameters params;
  params.deviceId = output.getDefaultOutputDevice();
  params.nChannels = 1;
  RtAudioFormat format = (sizeof(stk::StkFloat) == 8) ? RTAUDIO_FLOAT64 : RTAUDIO_FLOAT32;
  unsigned int audio_buffer = stk::RT_BUFFER_SIZE;
  output.openStream(&params, // output params
		    NULL,    // input params
		    format,  // audio format
		    (unsigned int)stk::Stk::sampleRate(),
		    &audio_buffer, 
		    &tick, 
		    (void *)guitar);
  output.startStream();


  // init instrument
  guitar = new Strings(6);

  // set pitches
  // major bar chord
  guitar->set_relative_string_pitch(0, 0);
  guitar->set_relative_string_pitch(1, 7);
  guitar->set_relative_string_pitch(2, 12);
  guitar->set_relative_string_pitch(3, 16); // change to 15 for minor
  guitar->set_relative_string_pitch(4, 19);
  guitar->set_relative_string_pitch(5, 24);
  
  launch_gui(argc, argv, "Air Guitar!");

  // clean up and exit
  output.closeStream();
  delete guitar;
  return 0;
}

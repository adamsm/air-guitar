# Air Guitar! #

## Overview ##
Air Guitar! is an augmented reality guitar simulator. Simply stand in front of a Microsoft Kinect and rock out!

![playing_air_guitar.png](https://bitbucket.org/repo/R878a9/images/2333811750-playing_air_guitar.png)

Each string is simulated individually, using a custom extended version of the Karplus-Strong algorithm. The algorithm's parameters are set using the motion tracking data, so the strings have a physically-motivated response to velocity of attack and string length. The rest of this document explains the details of the implementation and how the project progressed. 

## Installation ##
Currently there is no installer, so the project must be built using Microsoft Visual Studio. Unfortunately, I've only had the opportunity to test it with MSVS Community 2013 on Windows 7, but there is no reason to suspect it won't compile on other MS platforms.

## Requirements ##
* [Synthesis Toolkit (STK) 4.5.0](https://ccrma.stanford.edu/software/stk/index.html) (included in this repository)
* [freeGLUT 2.8.1 MSVC version](http://www.transmissionzero.co.uk/software/freeglut-devel/) (.dll included in this repository)
* [Microsoft Kinect SDK v1.8](http://www.microsoft.com/en-us/download/details.aspx?id=40278)
* Microsoft Visual Studio

* * *
## Highlighted Features ##
* Visual representation of the strings and pick that is synced with the generated audio. This greatly improves user experience, since the Kinect v1 tracking data- even after filtering- is just not sufficient to have the pick be right where you would expect. The graphics allow you to see the low frame-rate of the Kinect and the lag of the filter, and people seem to quickly gain an intuition about controlling the movements of the pick.
* The pick-string collisions are handled so that plucks are only registered when there has been contact between the strings. 
* The pitch is changed by changing string length, just as on a real guitar. It is tuned to play a major bar chord, and the positions correspond enough with my bass-playing intuition that I can actually play some (poorly-tuned) songs.
* All of the guitar's components are scaled to match the user, as determined by shoulder width. The guitar position, strings, and bridge are set to align with my real-world bass, and I spent some time informally polling people of different statures to see if it felt right to them. (The use of a bass instead of a guitar actually doesn't matter; the only significant difference for this context would be the neck length, and the virtual guitar has an infinitely-long neck.)
* The plucking attack controls an additional low-pass filter on the noise burst, so that the sound responds to the impulse of the pluck (i.e. it is not simply changing the gain but also the tone).
* You can drop the guitar by lowering your left hand to where the neck is parallel to the ground and pick it back up again by moving the left hand back up.
* Combining old technologies into a new, fun package... it is fun to play! Smiles guaranteed when new users strum for the first time.

## Implementation Details ##
### Extended Karplus-Strong ###
The basic idea of the Karplus-Strong algorithm is to simulate the physical waves travelling up and down the strings, losing some energy each time they hit the ends. It accomplishes this by sending an initial noise burst through a delay line and a simply averaging filter. The samples in the delay line correspond to the samples of the wave travelling along the string, and the filter serves to dampen the signal at the ends of the string. It is a simple but highly-efficient and surprisingly good-sounding algorithm. Here is a diagram of the basic circuit, courtesy Wikipedia:

![Karplus-strong-schematic.png](https://bitbucket.org/repo/R878a9/images/831897022-Karplus-strong-schematic.png)

I used a couple of extensions, described first in the extensions paper in the works cited. First, there is a problem with the initial algorithm in that the integer length of the delay line means that pitches with fractional divided wavelength components cannot be accurately sampled- particularly high pitches. The solution is to use the delay of an all-pass filter in addition to the delay line to simulate the correct length. As another improvement, using a more advanced low-pass filter instead of simple bit averaging more accurately models the fact that high frequencies are the most dampened at the ends of the guitar, due to the frequency dependence of energy. I also added an extra low-pass filter after the initial noise burst, which allows for control over the attack of the pick.

For full details on the Karplus-Strong algorithm and a variety of extensions that can be made to it, see the works cited. 

### Kinect ###
I used skeleton tracking of the hands and shoulders, along with a filter to reduce the noise of the joint data.

### Pick-String Collisions ###
Efficient pick-string collisions were of particular interest. I needed a way to determine when and where a collision happened when both the string and pick were moving. The solution I eventually devised was to linearly transform the points into a space where the lowest string lies from 0 to its length along the x-axis. This space preserves distance calculations and greatly simplifies the calculation of intersections. All you have to do is check the x-intercept of the line determined by the pick's before and after positions in string-space and then check whether the intercept falls in the length of the string at the time of intersection. (In practice, the simplifying assumption that the string length didn't change improves performance without any apparent sacrifice in simulation quality.) This technique also offers the advantage of being able to efficiently check for intersections with the other strings without doing extra transformations, as they will just be offset from the x-axis by some integer factor of the distance between strings.

## Project Progress ##
This project originally began with the idea to track a person's feet an simulate the audio of their footsteps, such as in:

* Cook. "Modelling Bill’s Gait: Analysis and Parametric Synthesis of Walking Sounds." AES 22nd International Conference on Virtual, Synthetic and Entertainment Audio.
* Turchet, Stefania, Smilen, and Rolf. "Physically Based Sound Synthesis and Control of Footsteps Sounds." Proceedings of the 13th International Conference on Digital Audio Effects (DAFx-10).

However, as work progressed and I learned about Kinect development, it became clear that the tracking data was insufficient for capturing any sort of detail. (Side note: the Virtual Footsteps repository has been deleted, but should redirect to this one.) Indeed, most systems that model footsteps resynthesize audio data from the feet or use shoes with installed pressure sensors. I had been playing around with string simulations in my spare time, got really excited about the idea of a virtual guitar, and then this project took over.

### Challenges ###
Learning the technologies involved was a large hurdle. These were all new to me:

* STK
* Kinect SDK
* Developing on Windows (and Visual Studio)

In some ways this is also my first C++ project, as I've only ever used it before for small bits of performance-intensive code, and I hadn't done much with OpenGL before either.

I also had to learn a lot of signal processing to understand the theory behind the project. Many hours were spent watching YouTube recordings of signal processing lectures to enable me to understand STK's filter components.

On the algorithmic side, efficiently computing pick-string collisions took a lot of thought.

Also, tuning the parameters (eg. low-pass cutoff on the filters, distance between strings as a function of shoulder width) to find the "best" string sounds was difficult, since it is ultimately subjective. I'd recommend contributing to this project for anyone who gets restless from sitting in chairs too long; you have to stand up every couple minutes to test the code!


## Future Work ##
There are many cool extensions to make. Here are a few:

* Upgrade to Kinect v2 and use color data to get better hand tracking. 
* Use modal synthesis techniques to model the body of an acoustic guitar.
* Further extend the Karplus-Strong algorithm.
* Add a GUI to change the chord shape being played. (The framework is already in place; there just ins't an interactive way to call the change chord methods yet.)

## Additional Links ##
* [Slides](https://drive.google.com/file/d/0B9I__SCY1oSccU1MeFFhMWdsUnpWTHpGcXlSNldVN1Q5M1FB/view?usp=sharing) from a lecture I gave on Karplus-Strong string simulation methods. Most of the lecture was spent explaining the derivation of the 1D wave equation solutions on the whiteboard and then how Karplus-Strong imitates the behavior described in those solutions.
* [Slides](https://docs.google.com/a/cs.unc.edu/presentation/d/11CoUWsPiPA02c3sduDdpLwqoZcQnLPp-fEg-t36ozC4/edit?usp=sharing) from the class presentation I gave demoing this project.

## Thanks ##
* [Dr. Ming Lin](http://www.cs.unc.edu/~lin/) for teaching COMP 768: Physically-based Modelling and Simulation.
* [Dr. Henry Fuchs](http://henryfuchs.web.unc.edu/) for lending me a Kinect from his lab.

## Works Cited ##
Jaffe and Smith. "Extensions of the Karplus-Strong Plucked-String Algorithm." Computer Music Journal, Vol. 7, No. 2.

Kevin Karplus, Alex Strong “Digital Synthesis of Plucked String and Drum Timbres”. Computer Music Journal (MIT Press) 7 (2): 4355